#!/bin/bash

for i in 1,1 2,1 4,2 8,2 16,4 32,12
do
  echo "Scheduling job for ${i%,*} nodes (${i#*,} ps): "
  for net in inception3 alexnet resnet50 resnet152 vgg19
  do 
    echo -n "  $net... "
    sbatch -N ${i%,*} google-benchmarks_dist_daint.sh ${i%,*} ${i#*,} parameter_server true $net 32
  done
done
