# Distributed TensorFlow benchmarks

This is a fork of [e-bug/distributed-tensorflow-benchmarks](https://github.com/e-bug/distributed-tensorflow-benchmarks)
containing extended scripts for running a variety of machine learning benchmarks
on the Piz Daint supercomputer.

---

 [TF 1.1.0 Data Spreadsheet](https://docs.google.com/spreadsheets/d/1u4LlBYWodwVQqO45LMiJbNRXzcGnmpnfX-vDyfFkgAA/edit?usp=sharing)


## Description of this repository

- `environments_setup/`: folder containing scripts to quickly setup a local workstation, Piz Daint and AWS EC2 instances to run TensorFlow.
- `run_jupyter_notebooks/`: folder containing scripts to easily start a Jupyter notebook on a local workstation, Piz Daint or an AWS EC2 instance.
- `distributed_tensorflow_launchers/`: folder containing scripts to promptly launch a TensorFlow application across multiple processes on a local workstation, Piz Daint or AWS EC2 instances.
- `MNIST/`: folder containing TensorFlow's deep MNIST tutorial and two variations: a GPU-enhanced version that allows to specify data formats (NCHW or NHWC) and a distributed version of this one.
- `google-benchmarks/`: folder containing the scripts used for running Google benchmarks on Piz Daint
- `report/`: folder containing the report for the summer internship at CSCS. 
- `presentation/`: folder containing the slides for the end-of-internship seminar given at CSCS.
